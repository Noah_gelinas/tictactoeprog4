namespace tictactoe;

    class TicTacToeGrid {
        TicTacToePosition[,] Grid {get;set;}
        public TicTacToeGrid() {
            Grid = new TicTacToePosition[3,3];
            for(int i=0;i<Grid.GetLength(0);i++) {
                for(int j=0;j<Grid.GetLength(0);j++) {
                    Grid[i,j]=new TicTacToePosition();
                }
            }
        }

        
        public void PlaceCharacter(TicTacToeMarks character,int row, int col) {
            try{
                Grid[col,row].Mark=character;
            }catch(Exception e){    
            }
        }

        public void PrintGrid() {
            
            for(int i=0;i<Grid.GetLength(0);i++) {
                for(int j=0;j<Grid.GetLength(0);j++) {
                    Console.Write(Grid[i,j]);
                    if(j<2) {
                        Console.Write(" | ");
                    }
                }
                Console.WriteLine();
            }
        }

        public string CheckRows() {
            string curr;
            string winner=" ";
            for(int i=0;i<Grid.GetLength(0);i++) {
                curr=Grid[i,0].ToString();
                if(curr==Grid[i,1].ToString() && curr==Grid[i,2].ToString()) {
                    winner=Grid[i,0].ToString();
                }
            }
            return winner;
        }

        public string CheckCol() {
            string curr;
            string winner=" ";
            for(int i=0;i<Grid.GetLength(0);i++) {
                curr=Grid[0,i].ToString();
                if(curr==Grid[1,i].ToString() && curr==Grid[2,i].ToString()) {
                    winner=Grid[0,i].ToString();
                }
            }
            return winner;
        }

        public string CheckDiagonals() {
            string curr1;
            string curr2;
            string winner=" ";
            int i=0;
                curr1=Grid[i,0].ToString();
                curr2=Grid[i,2].ToString();
                if(curr1==Grid[i+1,1].ToString() && curr1==Grid[i+2,2].ToString()) {
                    winner=Grid[0,i].ToString();
                }else if(curr2==Grid[i+1,1].ToString() && curr2 == Grid[i+2,0].ToString()) {
                    winner=Grid[i,2].ToString();
                }
            
            return winner;
        }

        public string CheckGrid(TicTacToeGrid gridObj) {
            
            string winner=" ";
            winner = gridObj.CheckRows();
            if (String.Equals(winner,"X") || String.Equals(winner,"O")) {
                return winner;       
            }else if(String.Equals(winner," ")) {
                winner = gridObj.CheckCol();
                if (String.Equals(winner,"X") || String.Equals(winner,"O")) {
                    return winner;
                }
            }

            if(String.Equals(winner," ")) {     
                winner = gridObj.CheckDiagonals();
                if(String.Equals(winner,"X") ||String.Equals(winner,"O")) {
                    return winner;
                }
            }
            
            for(int i=0;i<Grid.GetLength(0);i++) {
                for (int j=0;j<Grid.GetLength(0);j++){
                    if (Grid[i,j].Mark==TicTacToeMarks.Empty) {
                        return winner;
                    }
                }
            }
            return "tie";             
        }
    }
