namespace tictactoe {

    class TicTacToeGame {
        public static void Main(String[] args) {
            bool valid=false;
            int col=0;
            int row=0;
            string character=" ";
            TicTacToeGrid gridObj= new TicTacToeGrid();
            bool gameOver=false;
            while(gameOver==false) {
                Console.WriteLine("Enter a column number between 0-2");
                col = Convert.ToInt32(Console.ReadLine());
                while (valid==false) {
                    if (col<0 || col > 2) {
                        Console.WriteLine("Incorrect input! enter a number between 0-2");
                        col=Convert.ToInt32(Console.ReadLine());
                    }else{
                        valid=true;
                    }
                }
                Console.WriteLine("Enter a row number between 0-2");
                row = Convert.ToInt32(Console.ReadLine());
                valid=false;
                while (valid==false) {
                    if (row<0 || row > 2) {
                        Console.WriteLine("Incorrect input! enter a number between 0-2");
                        row=Convert.ToInt32(Console.ReadLine());
                    }else{
                        valid=true;
                    }
                }

                Console.WriteLine("Enter Character to Place 'X' or 'O'");
                character =Console.ReadLine();
                TicTacToeMarks marks=TicTacToeMarks.Empty;
                valid=false;
                while(valid==false) {
                    if(character!="X" && character!="O") {
                        Console.WriteLine("Incorrect input! please only enter 'X' or 'O'");
                        character = Console.ReadLine();
                    }else if(character.Equals("X")) {
                        marks=TicTacToeMarks.X;
                        valid=true;
                    }else if(character.Equals("O")){
                        marks=TicTacToeMarks.O;
                        valid=true;
                    }
                }
                gridObj.PlaceCharacter(marks,row,col);

                if (String.Equals(gridObj.CheckGrid(gridObj)," ")) {
                    Console.WriteLine("Ongoing game!");
                    gridObj.PrintGrid();
                    gameOver=false;
                }else if (String.Equals(gridObj.CheckGrid(gridObj),"tie")){
                    gridObj.PrintGrid();
                    Console.WriteLine("The game has ended in a tie!");
                    gameOver=true;
                }else{
                    gridObj.PrintGrid();
                    Console.WriteLine(gridObj.CheckGrid(gridObj)+ " has won the game!");
                    gameOver=true;
                }
            }
        }
    }
}