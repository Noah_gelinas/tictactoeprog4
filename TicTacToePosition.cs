namespace tictactoe;

public class TicTacToePosition {
    private TicTacToeMarks _mark;
    public TicTacToePosition() {
        _mark=TicTacToeMarks.Empty;
    }
    public TicTacToeMarks Mark {
        get { return _mark; }
        set {   if(_mark==TicTacToeMarks.X || _mark==TicTacToeMarks.O){
                    throw new ArgumentException("Invalid Input");
                }else if(value==TicTacToeMarks.X || value==TicTacToeMarks.O || value==TicTacToeMarks.Empty) {
                    _mark=value;
                }else{
                    throw new ArgumentException("Invalid Input");
                }
        }
    }

    public override string ToString () {
        if (_mark==TicTacToeMarks.X) {
            return "X";
        }else if(_mark==TicTacToeMarks.O) {
            return "O";
        }else{
            return " ";
        }
    }

}